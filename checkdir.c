#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include "crc8.h"
#include "checkdir.h"
#include "def.h"

void checkDir(const char *dirName, file_t **oldArr, file_t **newArr)
{
    int i = 0;
    int j = 0;
    checkDir_(dirName, oldArr, newArr, &i);
    while ( NULL != oldArr[j]) {
        if (!oldArr[j]->isFound)
            printf("NOTE! Removed: %s\n", oldArr[j]->name);
        ++j;
    }
}

void checkDir_(const char *dirName, file_t **oldArr, file_t **newArr, unsigned int *pInd)
{
    char filePath[MAX_PATH];
    struct dirent *entry;
    unsigned int i = *pInd;
    DIR *dir = opendir(dirName);
    if (NULL == dir) {
        fprintf(stderr, "Can't open %s\n", dirName);
        exit(1);
    }
    while ( NULL != (entry = readdir(dir)) ) {
        snprintf(filePath, MAX_PATH, "%s/%s", dirName, entry->d_name);
        if (entry->d_type & DT_DIR) {
            if (strcmp (entry->d_name, "..") != 0 &&
                strcmp (entry->d_name, ".") != 0)
            {
                checkDir_(filePath, oldArr, newArr, &i);
            }
        }
        else {
                int n;
                //printf("%s\n", filePath);
                newArr[i] = malloc(sizeof(file_t));
                newArr[i]->crc = getFileCrc8(filePath);
                strncpy(newArr[i]->name, filePath, MAX_PATH);
                newArr[i]->isFound = 0;
                n = findFile(newArr[i]->name, oldArr);
                if (-1 == n) {
                    printf("NOTE! Created: %s\n", newArr[i]->name);
                }
                else if (newArr[i]->crc != oldArr[n]->crc) {
                    printf("NOTE! Changed: %s\n", newArr[i]->name);
                }
                ++i;
        }        
    }
    newArr[i] = NULL;
    closedir(dir);
    *pInd = i;
}

void delRecords(file_t **arr)
{
    int i = 0;
    while (NULL != arr[i]) {
        free(arr[i]);
        ++i;
    }
}

int findFile(const char *fileName, file_t **arr)
{
    int i = 0;
    while ( NULL != arr[i]) {
        if( !strcmp(fileName, arr[i]->name) ) {
            arr[i]->isFound = 1;
            return i;
        }
        ++i;
    }
    return -1;
}
