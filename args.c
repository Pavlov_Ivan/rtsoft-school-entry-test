#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "args.h"
#include "def.h"

void getArgs (int argc, char *argv[], 
        char dirNames[][MAX_PATH],
        unsigned int *pt, unsigned int *pdirNum) {
    unsigned int dirNum = *pdirNum;
    while (--argc > 0) {
        ++argv;
        if (!strcmp(*argv, "-t")) {
            ++argv;
            --argc;
            if(!sscanf(*argv, "%u", pt))
                fprintf(stderr, "Usage: [-t time] [-d directory]\n");
        }
        else if (!strcmp(*argv, "-d")) {
            while (--argc > 0 && '-' != (*++argv)[0]) {
                //int len;
                strncpy(dirNames[dirNum], *argv, MAX_PATH);
                //len = strlen(dirNames[dirNum]);
                //if(len>1 && '/' == dirNames[dirNum][len-1])
                //    dirNames[dirNum][len-1] = '\0';
                cutSlash(dirNames[dirNum]);
                ++dirNum;
            }
            --argv;
            ++argc;
        }
        else {
            fprintf(stderr, "Usage: [-t time] [-d directory]\n");
            exit(1);
        }
    }
    *pdirNum = dirNum;
}

void cutSlash(char *str)
{
    int len = strlen(str);
    if(len>1 && '/' == str[len-1])
        str[len-1] = '\0';
}
