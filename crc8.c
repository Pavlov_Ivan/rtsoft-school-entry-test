#include <stdio.h>
#include <stdlib.h>
#include "crc8.h"

#define BUFF_SIZE 512

unsigned char getFileCrc8(const char * fileName)
{
    FILE * fd = fopen(fileName, "r");
    unsigned char buff[BUFF_SIZE];
    size_t len;
    unsigned char crc = 0xFF;
    if (NULL == fd) {
        fprintf(stderr, "Can't open file %s\n", fileName);
        exit(1);
    }
    while (!feof(fd)) {
        len = fread(buff, 1, sizeof(buff), fd);
        crc = crc8(buff, len, crc);
    }
    fclose(fd);
    return crc;
}


unsigned char crc8(const unsigned char *pcBlock, size_t len, unsigned char crc)
{
    //unsigned char crc = 0xFF;
    unsigned int i;

    while (len--) {
        crc ^= *pcBlock++;

        for (i = 0; i < 8; i++)
            crc = crc & 0x80 ? (crc << 1) ^ 0x31 : crc << 1;
    }
    return crc;
}
