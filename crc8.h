#ifndef CRC8_H_
#define CRC8_H_

unsigned char getFileCrc8(const char * fileName);
unsigned char crc8(const unsigned char *pcBlock, size_t len, unsigned char crc);

#endif //CRC8_H_
