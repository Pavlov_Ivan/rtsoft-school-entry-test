#ifndef CHECKDIR_H_
#define CHECKDIR_H_

#include "def.h"

typedef struct file {
    char name[MAX_PATH];
    unsigned char crc;
    int isFound;
} file_t;

void checkDir(const char *dirName, file_t **oldArr, file_t **newArr);
int findFile(const char *fileName, file_t **arr);
void delRecords(file_t **arr);
void checkDir_(const char *dirName, file_t **oldArr, file_t **newArr, unsigned int *pInd);
#endif //CHECKDIR_H_
