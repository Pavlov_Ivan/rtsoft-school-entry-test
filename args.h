#ifndef ARGS_H_
#define ARGS_H_

#include "def.h"

void getArgs (int argc, char *argv[], 
        char dirNames[][MAX_PATH],
        unsigned int *pt, unsigned int *pdirNum);
void cutSlash (char *str);
#endif //ARGS_H_
