#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "crc8.h"
#include "checkdir.h"
#include "args.h"
#include "def.h"


typedef struct thrdArg {
    unsigned int t;
    char dirName[MAX_PATH];
    int stopFlag;
} thrdArg_t; 

void lookAtDirLoop(const char *dirName, unsigned int period, int *stopFlag)
{
    file_t **newArr = calloc(MAX_FILES, sizeof(file_t *));
    file_t **oldArr = calloc(MAX_FILES, sizeof(file_t *));
    file_t **tmp;
    FILE *devNull = fopen("/dev/null", "w");
    FILE *stdout_ = stdout;
    int first = 1;
    if (NULL == devNull) {
        perror("Can't open /dev/null\n");
        exit(1);
    }
    while (!*stopFlag) {
        stdout = first ? devNull : stdout_;
        first = 0;
        checkDir(dirName, oldArr, newArr);
        delRecords(oldArr);
        sleep(period);
        tmp = oldArr;
        oldArr = newArr;
        newArr = tmp;
    }
    free(oldArr);
    free(newArr);
    fclose(devNull);
}

void *thread_func(void *arg)
{
    thrdArg_t *p = arg;
    lookAtDirLoop(p->dirName, p->t, &p->stopFlag);
}

void addDir(pthread_t *threads, thrdArg_t *thrdArgs, 
            char *dirName, unsigned int t)
{
    int i;
    for (i = 0; i<MAX_DIRS; ++i) {
        if (1 == thrdArgs[i].stopFlag) {
            //printf("%s %d\n", dirName, i);
            thrdArgs[i].t = t;
            thrdArgs[i].stopFlag = 0;
            strncpy(thrdArgs[i].dirName, dirName, MAX_PATH);
            pthread_create(&threads[i], NULL, thread_func, &thrdArgs[i]);
            break;
        }
    }
}

void forgetDir(char *dirName, pthread_t *threads, thrdArg_t *thrdArgs)
{
    int i;
    for(i = 0; i<MAX_DIRS; ++i)
    {
        if(!thrdArgs[i].stopFlag && !strcmp(thrdArgs[i].dirName, dirName))
        {
            thrdArgs[i].stopFlag = 1;
            pthread_join(threads[i], NULL);
            break;
        }
    }
}

int main(int argc, char *argv[])
{
    unsigned int t = 10;
    unsigned int dirNum = 0;
    unsigned int i;
    char dirNames[MAX_DIRS][MAX_PATH];
    pthread_t threads[MAX_DIRS];
    thrdArg_t *thrdArgs = malloc(MAX_DIRS*sizeof(thrdArg_t));
    getArgs (argc, argv, dirNames, &t, &dirNum);
    for (i = 0; i<MAX_DIRS; ++i) {
        thrdArgs[i].stopFlag = 1;
    }
    for (i = 0; i<dirNum; ++i) {
        //printf("%s\n", dirNames[i]);
        //thrdArgs[i].t = t;
        //thrdArgs[i].dirName = dirNames[i];
        //pthread_create(&threads[i], NULL, thread_func, &thrdArgs[i]);
        addDir(threads, thrdArgs, dirNames[i], t);
    }
    while (1) {
        char cmd[16];
        char arg[1024];
        char buff[1024];
        int res;
        fgets(buff, 1024, stdin);
        res = sscanf(buff, "%s %s", cmd, arg);
        if (2 == res) {
            if (!strcmp(cmd, "add")) {
                cutSlash(arg);
                addDir(threads, thrdArgs, arg, t);
            }
            else if (!strcmp(cmd, "forget")) {
                cutSlash(arg);
                forgetDir(arg, threads, thrdArgs);
            }
            else {
                fprintf(stderr, "Unknown command\n");
            }
        }
        else if (1 == res && !strcmp(cmd, "quit")) {
            for (i = 0; i<MAX_DIRS; ++i)
                thrdArgs[i].stopFlag = 1;
            break;
        }
        else {                                   
            fprintf(stderr, "Unknown command\n");
        }

    }
    for (i = 0; i<MAX_DIRS; ++i) {
        pthread_join(threads[i], NULL);
    }
    free(thrdArgs);
    return 0;
}
