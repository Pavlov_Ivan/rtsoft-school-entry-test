CC=gcc

default: main.o crc8.o checkdir.o args.o
	$(CC) main.o crc8.o checkdir.o args.o -o lookat -lpthread

main.o: main.c
	$(CC) -c main.c -D_REENTRANT

crc8.o: crc8.c
	$(CC) -c crc8.c -D_REENTRANT

checkdir.o: checkdir.c
	$(CC) -c checkdir.c -D_REENTRANT

args.o: args.c
	$(CC) -c args.c

clean:
	rm *.o
